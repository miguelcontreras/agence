<?php

Route::get('/', [
    'as' => 'home',
    'uses' => 'Frontend\PagesController@index'
]);

Route::get('consultant/data_line_bar.xml', [
    'as'   => 'consultant.graph',
    'uses' => 'Backend\ConsultantController@graph',
]);

Route::get('consultant/data_pizza.xml', [
    'as'   => 'consultant.graph',
    'uses' => 'Backend\ConsultantController@pizza',
]);
