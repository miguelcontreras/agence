<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cao_usuario';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
    
    #has one
    public function permission()
    {
        return $this->hasOne('App\Models\Permission', 'co_usuario', 'co_usuario');
    }
    
    #has one
    public function salary()
    {
        return $this->hasOne('App\Models\Salary', 'co_usuario', 'co_usuario');
    }
    
    #has many
    public function serviceOrders()
    {
        return $this->hasMany('App\Models\ServiceOrder', 'co_usuario', 'co_usuario');
    }
}
