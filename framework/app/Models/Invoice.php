<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'cao_fatura';
    
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}

