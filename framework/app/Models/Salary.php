<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = 'cao_salario';
    
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}

