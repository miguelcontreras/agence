<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceOrder extends Model
{
    protected $table = 'cao_os';
    
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
    
    #has many
    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice', 'co_os', 'co_os');
    }
}

