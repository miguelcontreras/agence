<?php

function delete_cookie($name)
{
    \Cookie::queue(\Cookie::forget($name));
}

function make_cookie($name, $value, $expire = 60)
{
    \Cookie::queue(\Cookie::make($name, $value, $expire));
}

function get_cookie($name)
{
    return \Request::cookie($name);
}