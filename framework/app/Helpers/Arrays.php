<?php

function count_menus($modules)
{
    $count = 0;
    foreach($modules as $module)
    {
        if(is_true($module['is_menu']))
        {
            $count++;
        }
    }
    
    return $count;
}

function objects_to_array($array_objects)
{
    $array = [];
    
    foreach($array_objects as $object)
    {
        array_push($array,(array) $object);
    }
    
    return $array;
}

function array_to_string($array)
{
    #se convierte el array en formato viejo ( array( 0 => array (...) ) )
    $content = preg_replace('#,(\s+|)\)#', '$1)', var_export($array, true));
    
    #remover todas las ocurrencias de ( 0 => array )
    $content = preg_replace('/\s+[0-9]*\s+\=\>\s+(array)\s+/', '', $content);
    
    $content = str_replace('(', '[', $content);
    $content = str_replace(')', ']', $content);
    $content = str_replace('array', '', $content);
    $content = str_replace('\'\'', 'null', $content);
    
    return $content;
}

function array_columns($array,$columns)
{
    $new_array = [];
    
    foreach($array as $index_1 => $item)
    {
        foreach($item as $index_2 => $value)
        {
            if(in_array($index_2,$columns))
            {
                $new_array[$index_1][$index_2] = $value;
            }
        }
    }
    
    return $new_array;
}

function multiple_in_array($values,$array)
{
    $found = false;
    
    foreach($array as $item)
    {
        if(in_array($item,$values))
        {
            $found = true;
            
            break;
        }
    }
    
    return $found;
}

function unset_index($keys,$array)
{
    if(is_array($keys))
    {
        foreach($keys as $key)
        {
            if(isset($array[$key]))
            {
                unset($array[$key]);
            }
        }
    }
    else
    {
        if(isset($array[$keys]))
        {
            unset($array[$keys]);
        }
    }
    
    return $array;
}