<?php

function sync_elogger_users()
{
    $wsAdempiereUrl = 'http://ews1.esvenca.com:8000/';
    
    $adempiere_users = \DB::connection('adempiere')
                          ->table('c_bpartner as cbp')
                          ->select('cbp.firstname1 AS first_name',
                                   'cbp.lastname1 AS last_name',
                                   'cbp.c_bpartner_id as c_bpartner_id',
                                   'cbp.value AS value',
                                   'hp.email_employee AS email',
                                   'hp.genero AS gender',
                                   \DB::raw('split_part(hp.email_employee, \'@\', 1) AS username'),
                                   'hj.name AS job',
                                   'hd.hr_department_id AS department_id',
                                   'hd.name AS department',
                                   'hel.phone1 AS phone',
                                   'hel.urbanization AS address')
                          ->join('hr_employee AS he', 'he.c_bpartner_id', '=', 'cbp.c_bpartner_id')
                          ->join('ad_image AS ai', 'cbp.logo_id', '=', 'ai.ad_image_id', 'left')
                          ->join('hr_personal AS hp', 'cbp.c_bpartner_id', '=', 'hp.c_bpartner_id')
                          ->join('hr_job AS hj', 'he.hr_job_id', '=', 'hj.hr_job_id')
                          ->join('hr_department AS hd', 'hd.hr_department_id', '=', 'he.hr_department_id')
                          ->join('hr_employee_location AS hel', 'hel.c_bpartner_id', '=', 'cbp.c_bpartner_id')
                          ->where('cbp.isemployee', '=', 'Y')
                          ->where('he.isactive', '=', 'Y')
                          ->whereNotNull('hp.email_employee')
                          ->orderBy('cbp.created', 'asc')
                          ->get()
                          ->toArray();
    
    $emails_adempiere_users = array_unique(array_column($adempiere_users, 'email'));
    $intranet_users         = \App\Models\User::all()->groupBy('email');
    $users_table            = \App\Models\User::getTableName();
    
    $delete_users  = [];
    $restore_users = [];
    $create_users  = [];
    
    /*$corporate_extensions = corporateExtensions();*/
    
    #para crear o modificar usuarios existentes
    foreach($adempiere_users as $user)
    {
        $extension = getUserExtension($user->username);
        
        if(isset($intranet_users[$user->email]))
        {
            $intranet_user = $intranet_users[$user->email][0];
            
            if(
                $intranet_user->first_name != $user->first_name ||
                $intranet_user->last_name != $user->last_name ||
                $intranet_user->value != $user->value ||
                $intranet_user->job != $user->job ||
                $intranet_user->department != $user->department ||
                $intranet_user->phone != $user->phone ||
                $intranet_user->address != $user->address ||
                $intranet_user->extension != $extension ||
                ( $intranet_user->gender == 'male' && strtoupper($user->gender) != 'M' || $intranet_user->gender == 'female' && strtoupper($user->gender) != 'F' )
            )
            {
                $new_values = [ 'updated_at' => date('Y-m-d H:i:s') ];
                
                if($intranet_user->first_name != $user->first_name)
                {
                    $new_values['first_name'] = $user->first_name;
                }
                
                if($intranet_user->last_name != $user->last_name)
                {
                    $new_values['last_name'] = $user->last_name;
                }
                
                if($intranet_user->value != $user->value)
                {
                    $new_values['value'] = $user->value;
                }
                
                if($intranet_user->job != $user->job)
                {
                    $new_values['job'] = $user->job;
                }
                
                if($intranet_user->department != $user->department)
                {
                    $new_values['department'] = $user->department;
                }
                
                if($intranet_user->phone != only_numbers($user->phone))
                {
                    $new_values['phone'] = only_numbers($user->phone);
                }
                
                if($intranet_user->address != $user->address)
                {
                    $new_values['address'] = $user->address;
                }
                
                if($intranet_user->extension != $extension)
                {
                    $new_values['extension'] = $extension;
                }
                
                if($intranet_user->gender == 'male' && strtoupper($user->gender) != 'M' || $intranet_user->gender == 'female' && strtoupper($user->gender) != 'F')
                {
                    $new_values['gender'] = strtoupper($user->gender) == 'F' ? 'female' : 'male';
                    
                    if(custom_contains('default.png', $user->image))
                    {
                        $new_values['image'] = strtoupper($user->gender) == 'F' ? 'female-default.png' : 'male-default.png';
                    }
                }
                
                #actualizar
                \DB::table($users_table)
                   ->where('username', '=', $intranet_user->username)
                   ->update($new_values);
            }
            
            if(!is_null($intranet_user->deleted_at))
            {
                array_push($restore_users, $intranet_user->toArray());
            }
        }
        else
        {
            $image = callAPI($wsAdempiereUrl.'employees/getImage/'.$user->email);
            
            array_push($create_users, [
                'c_bpartner_id' => $user->c_bpartner_id,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'value'         => $user->value,
                'email'         => $user->email,
                'username'      => $user->username,
                'job'           => $user->job,
                'department'    => $user->department,
                'extension'     => $extension,
                'gender'        => strtoupper($user->gender) == 'F' ? 'female' : 'male',
                'address'       => $user->address,
                'phone'         => $user->phone,
                'image'         => has_something($image['data']['image']) ? base64_to_image($user->username, 'png', 'images/users/', $image['data']['image']) : ( strtoupper($user->gender) == 'F' ? 'female-default.png' : 'male-default.png' ),
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);
        }
    }
    
    #para eliminar usuarios que ya no esten en adempiere
    foreach($intranet_users as $user)
    {
        $user = $user[0];
        
        if(!in_array($user->email, $emails_adempiere_users) && is_null($user->deleted_at))
        {
            array_push($delete_users, $user->toArray());
        }
    }
    
    $now = date('Y-m-d H:i:s');
    
    #crear
    if(count($create_users) > 0)
    {
        \DB::table($users_table)->insert($create_users);
        
        $role_basic_user = \DB::table('roles')->where('code', '=', 'basic-user')->first();
        
        if(!is_null($role_basic_user))
        {
            $created_users = \DB::table($users_table)
                                ->whereIn('username', array_column($create_users, 'username'))
                                ->whereNull('deleted_at')
                                ->get();
            
            \DB::table('role_user')->insert($created_users->map(function($user) use ($role_basic_user, $now){
                return [
                    'user_id'    => $user->id,
                    'role_id'    => $role_basic_user->id,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            })->toArray());
            
            foreach($created_users as $user)
            {
                generate_user_access($user->id);
            }
        }
    }
    
    #restaurar
    if(count($restore_users) > 0)
    {
        \DB::table($users_table)
           ->whereIn('id', array_column($restore_users, 'id'))
           ->update([ 'deleted_at' => null ]);
        
        $role_basic_user = \DB::table('roles')->where('code', '=', 'basic-user')->first();
        
        if(!is_null($role_basic_user))
        {
            \DB::table('role_user')->insert(array_map(function($user) use ($role_basic_user, $now){
                return [
                    'user_id'    => $user['id'],
                    'role_id'    => $role_basic_user->id,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }, $restore_users));
            
            foreach($restore_users as $user)
            {
                generate_user_access($user['id']);
            }
        }
    }
    
    #eliminar
    if(count($delete_users) > 0)
    {
        \DB::table($users_table)
           ->whereIn('id', array_column($delete_users, 'id'))
           ->update([ 'deleted_at' => date('Y-m-d H:i:s') ]);
        
        \DB::table('role_user')
           ->whereIn('user_id', array_column($delete_users, 'id'));
        
        foreach($delete_users as $user)
        {
            generate_user_access($user['id']);
        }
    }
    
    return [
        'created'  => array_columns($create_users, [ 'first_name', 'last_name', 'email' ]),
        'restored' => array_columns($restore_users, [ 'first_name', 'last_name', 'email' ]),
        'deleted'  => array_columns($delete_users, [ 'first_name', 'last_name', 'email' ]),
    ];
}

function getUserExtension($username)
{
    try
    {
        $ldap_con = ldap_connect(env('LDAP_HOST'), env('LDAP_PORT'));
        
        ldap_set_option($ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);
        
        if(ldap_bind($ldap_con, env('LDAP_USER'), env('LDAP_PASSWORD')))
        {
            $filter = '(&(objectClass=user)(objectCategory=person)(sAMAccountName='.$username.'))';
            $result = ldap_search($ldap_con, 'ou=ESVENCA,dc=esvenca,dc=local', $filter, [ 'telephonenumber' ]) or exit('no se conecto');
            
            $entries = ldap_get_entries($ldap_con, $result);
            
            if($entries['count'] > 0 && $entries[0]['count'] > 0)
            {
                return $entries[0]['telephonenumber'][0];
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    catch(\Exception $e)
    {
        return null;
    }
}

function get_user_from_header(Request $request) : \App\Models\User
{
    $token    = $request->header('x-auth-token');
    $username = $request->header('x-auth-uid');
    
    return \App\Models\User::where('username', $username)
                           ->where('token', $token)
                           ->first();
}

function add_log($data, $user = null, $log = null, $action = null, $table = null, $register = null)
{
    $activity = new App\Models\Log();
    
    try
    {
        if(!is_array($user))
        {
            $user = json_decode($user, true);
            
            if(!has_something($user['username']) || !has_something($user['name']))
            {
                $user = null;
            }
        }
    }
    catch(\Exception $e)
    {
        $user = null;
    }
    
    if(!is_null($user) && count($user) > 1)
    {
        $activity->user_id = $user['id'];
        $activity->user    = $user['username'];
    }
    else
    {
        $activity->user_id = 0;
        $activity->user    = 'ROOT';
    }
    
    $activity->log = is_array($log) && count($log) > 0 ? json_encode($log) : null;
    /*$activity->activity = $data['activity'];*/
    $activity->icon     = $data['icon'];
    $activity->date     = date('Y-m-d H:i:s', time());
    $activity->action   = $action;
    $activity->table    = $table;
    $activity->register = $register;
    
    return secureSave($activity);
}

function module_structure($module_id = null)
{
    $modules = [];
    
    $result = \DB::table('modules as m')//module parent
                 ->distinct()
                 ->select('m.id as module_id', 'm.name as module_name', 'm.group_name as module_group_name', 'm.singular_title as module_singular_title', 'm.plural_title as module_plural_title', 'm.gender as module_gender', 'm.group as module_group', 'm.variable as module_variable', 'm.route as module_route', 'm.link as module_link', 'm.icon as module_icon', 'm.description as module_description', 'm.slug as module_slug', 'm.position as module_position', 'm.is_menu as module_is_menu', 'm.per_page as module_per_page', 'm.parent_id as module_parent_id');
    
    if(is_null($module_id))
    {
        $result = $result->whereNull('m.parent_id');
    }
    else
    {
        $result = $result->where('m.parent_id', '=', $module_id);
    }
    
    $result = $result->orderBy('m.position', 'asc')
                     ->orderBy('m.id', 'asc')
                     ->get();
    
    foreach($result as $item)
    {
        if(!isset($modules[$item->module_variable]))
        {
            $children = \App\Models\Module::where('parent_id', '=', $item->module_id)->count();
            
            $modules[$item->module_variable] = [
                'id'             => $item->module_id,
                'parent_id'      => $item->module_parent_id,
                'icon'           => $item->module_icon,
                'description'    => $item->module_description,
                'name'           => $item->module_name,
                'group_name'     => $item->module_group_name,
                'singular_title' => $item->module_singular_title,
                'plural_title'   => $item->module_plural_title,
                'gender'         => $item->module_gender,
                'group'          => $item->module_group,
                'variable'       => $item->module_variable,
                'slug'           => $item->module_slug,
                'position'       => $item->module_position,
                'per_page'       => $item->module_per_page,
                'route'          => $item->module_route,
                'link'           => $item->module_link,
                'is_menu'        => $item->module_is_menu == 'Y',
                'is_used'        => false,
                'children'       => $children > 0 ? module_structure($item->module_id) : [],
            ];
            
            if(is_null($module_id))//primer nivel
            {
                $modules[$item->module_variable]['is_used'] = false;
            }
        }
    }
    
    return $modules;
}

function parent_of_module($modules = [], $module_id = null)
{
    $result = \DB::table('modules as m')
                 ->distinct()
                 ->join('modules as m', 'm.id', '=', 'm.parent_id');
    
    if(is_null($module_id))
    {
        $result = $result->whereNull('m.parent_id');
    }
    else
    {
        $result = $result->where('m.parent_id', '=', $module_id);
    }
    
    $result = $result->where('ru.user_id', '=', $user_id)
                     ->orderBy('m.position', 'asc')
                     ->orderBy('rt.module_id', 'asc')
                     ->orderBy('rt.id', 'asc')
                     ->get();
    
    foreach($result as $item)
    {
        $children = \App\Models\Module::where('parent_id', '=', $item->module_id)->count();
        
        if(!isset($elements[$item->module_variable]))
        {
            $elements[$item->module_variable] = [
                'id'          => $item->module_id,
                'parent_id'   => $item->module_parent_id,
                'icon'        => $item->module_icon,
                'description' => $item->module_description,
                'name'        => $item->module_name,
                'variable'    => $item->module_variable,
                'slug'        => $item->module_slug,
                'position'    => $item->module_position,
                'per_page'    => $item->module_per_page,
                'route'       => $item->module_route,
                'is_menu'     => $item->module_is_menu == 'Y',
                'children'    => $children > 0 ? elements_for_user_access($user_id, $item->module_id) : [],
            ];
        }
        
        
        /*if($item->submodule_variable != $active_submodule)
        {
            $elements[$item->module_variable]['submodules'][$item->submodule_variable] = [
                'id'          => $item->submodule_id,
                'name'        => $item->submodule_name,
                'variable'    => $item->submodule_variable,
                'route'       => $item->submodule_route,
                'description' => $item->submodule_description,
                'database'    => $item->submodule_database,
                'where'       => $item->submodule_where,
                'per_page'    => $item->submodule_per_page,
                'position'    => $item->submodule_position,
                'is_summary'  => ( $item->submodule_is_summary == 'Y' ) ? true : false,
                'is_submenu'  => ( $item->submodule_is_submenu == 'Y' ) ? true : false,
                'can'         => [
                    'index'   => false,
                    'create'  => false,
                    'store'   => false,
                    'edit'    => false,
                    'update'  => false,
                    'destroy' => false,
                    'deeper'  => false,
                    'images'  => false,
                    'show'    => false,
                    'cancel'  => false,
                    'approve' => false,
                    'pay'     => false,
                    'history' => false,
                    'active'  => false,
                ],
                'routes'      => [],
            ];
            
            $active_submodule = $item->submodule_variable;
        }*/
    }
    
    return $elements;
}

function route_in_module_structure($route, &$module_structure = [], $can_list = null)
{
    $find = false;
    
    if(is_null($can_list))
    {
        $can_list = array_column(\DB::table('routes')
                                    ->distinct()
                                    ->select('variable')
                                    ->get()
                                    ->toArray(), 'variable');
    }
    
    foreach($module_structure as $module => $data)
    {
        if($data['id'] == $route->module_id)
        {
            $find = true;
            
            if(!isset($data['routes']))
            {
                $module_structure[$module]['routes'] = [];
            }
            
            if(!isset($data['can']))
            {
                $module_structure[$module]['can'] = [];
                
                foreach($can_list as $can)
                {
                    $module_structure[$module]['can'][$can] = false;
                }
            }
            
            array_push($module_structure[$module]['routes'], [
                'id'          => $route->route_id,
                'name'        => $route->route_name,
                'variable'    => $route->route_variable,
                'route'       => $route->route_route,
                'link'        => $route->route_link,
                'description' => $route->route_description,
                'slug'        => $route->route_slug,
            ]);
            
            foreach($can_list as $can)
            {
                $module_structure[$module]['can'][$can] = $route->route_variable == $can ? true : $module_structure[$module]['can'][$can];
            }
        }
        
        if(!$find && count($data['children']) > 0)
        {
            $find = route_in_module_structure($route, $module_structure[$module]['children'], $can_list);
        }
        
        if($find)
        {
            $module_structure[$module]['is_used'] = true;
            
            break;
        }
    }
    
    return $find;
}

function data_for_user_access($user_id, $modules_structure = [])
{
    $routes = [];
    
    $result = \DB::table('role_route as rr')
                 ->distinct()
                 ->select('rt.id as route_id', 'rt.module_id as module_id', 'rt.name as route_name', 'rt.link as route_link', 'rt.variable as route_variable', 'rt.route as route_route', 'rt.description as route_description', 'rt.slug as route_slug')
                 ->join('routes as rt', 'rt.id', '=', 'rr.route_id')
                 ->join('role_user as ru', 'ru.role_id', '=', 'rr.role_id')
                 ->where('ru.user_id', '=', $user_id)
                 ->orderBy('rt.module_id', 'asc')
                 ->orderBy('rt.id', 'asc')
                 ->get();
    
    foreach($result as $item)
    {
        route_in_module_structure($item, $modules_structure);
        
        array_push($routes, $item->route_route);
    }
    
    clean_module_structure($modules_structure);
    
    return [
        'routes'           => array_unique($routes),
        'elements'         => $modules_structure,
        'ordered_elements' => generate_ordered_elements($modules_structure),
    ];
}

function add_children_documentary_management(&$modules_structure, $user_id)
{
    
    $folders = \App\Models\GestionDocumental\Folder::whereNull('deleted_at')
                                                   ->whereNull('parent_id');
    
    $user_roles = array_column(\App\Models\User::find($user_id)->roles->toArray(), 'id');
    
    #si no es usuario root, entonces solo mostrarle a los que tiene permiso
    if(!in_array(1, $user_roles))
    {
        $folders = $folders->whereHas('relationViewers', function($query) use ($user_roles){
            return $query->whereIn('code', $user_roles)
                         ->whereNull('deleted_at');
        })
                           ->orWhereHas('relationAdmins', function($query) use ($user_roles){
                               return $query->whereIn('code', $user_roles)
                                            ->whereNull('deleted_at');
                           });
    }
    
    $folders = $folders->orderBy('position', 'asc')
                       ->get();
    
    foreach($folders as $index => $folder)
    {
        $modules_structure['documentary-management']['children'][$folder->slug] = [
            'id'             => $folder->id,
            'parent_id'      => $modules_structure['documentary-management']['id'],
            'icon'           => $modules_structure['documentary-management']['icon'],
            'description'    => $modules_structure['documentary-management']['description'],
            'name'           => $folder->name,
            'group_name'     => null,
            'singular_title' => null,
            'plural_title'   => null,
            'gender'         => null,
            'group'          => false,
            'variable'       => $folder->slug,
            'slug'           => $folder->slug,
            'position'       => $index,
            'per_page'       => 100,
            'route'          => 'documentary-management.index',
            'link'           => $modules_structure['documentary-management']['link'].'/'.$folder->slug,
            'is_menu'        => $modules_structure['documentary-management']['is_menu'],
            'is_used'        => $modules_structure['documentary-management']['is_used'],
            'children'       => [],
        ];
    }
}

function generate_ordered_elements(&$elements, &$ordered_elements = [])
{
    $index = 0;
    
    foreach($elements as $module => $data)
    {
        $elements[$module]['menus_length']    = count_menus($elements[$module]['children']);
        $ordered_elements[$index]             = $elements[$module];
        $ordered_elements[$index]['children'] = [];
        
        if(count($elements[$module]['children']) > 0)
        {
            generate_ordered_elements($elements[$module]['children'], $ordered_elements[$index]['children']);
        }
        
        $index++;
    }
    
    return $ordered_elements;
}

function clean_module_structure(&$module_structure = [])
{
    foreach($module_structure as $module => $data)
    {
        if(!$data['is_used'])
        {
            unset($module_structure[$module]);
        }
        else
        {
            if(count($module_structure[$module]['children']) > 0)
            {
                clean_module_structure($module_structure[$module]['children']);
            }
        }
    }
}

function generate_user_access($user_id)
{
    $data = data_for_user_access($user_id, module_structure());
    
    $permission = \App\Models\Permission::where('user_id', '=', $user_id)->first();
    
    if(is_null($permission))
    {
        $permission = new \App\Models\Permission();
        
        $permission->user_id = $user_id;
    }
    
    $permission->routes           = json_encode($data['routes']);
    $permission->elements         = json_encode($data['elements']);
    $permission->ordered_elements = json_encode($data['ordered_elements']);
    
    return secureSave($permission);
}

/*function user_permissions($roles_permissions)
{
    $user_permissions = [
        'routes' => [],
        'elements' => []
    ];
    
    foreach($roles_permissions as $index => $role_permissions)
    {
        $user_permissions['routes'] = array_merge($user_permissions['routes'],json_decode($role_permissions->routes));
        
        $elements = (array) json_decode($role_permissions->elements);
        
        foreach($elements as $module => $details_module)
        {
            $submodules = (array) $details_module->submodules;
            
            if(!isset($user_permissions['elements'][$module]))
            {
                $details_module->submodules = [];
                
                $user_permissions['elements'][$module] = (array) $details_module;
            }
            
            foreach($submodules as $submodule => $details_submodule)
            {
                $routes = $details_submodule->routes;
                $can = (array) $details_submodule->can;

                if(!isset($user_permissions['elements'][$module]->submodules[$submodule]))
                {
                    $details_submodule->routes = [];
                    $details_submodule->can = [];
        
                    $user_permissions['elements'][$module]['submodules'][$submodule] = (array) $details_submodule;
                }

                foreach($routes as $route)
                {
                    array_push($user_permissions['elements'][$module]['submodules'][$submodule]['routes'],(array) $route);
                }
                
                $user_permissions['elements'][$module]['submodules'][$submodule]['can'] = array_merge($user_permissions['elements'][$module]->submodules[$submodule]->can,$can);
            }
            
            dd($user_permissions['elements']);
        }
    }
    dd('ghola');
    $user_permissions['routes'] = array_unique($user_permissions['routes']);
}*/

function sync_up_all_users()
{
    $users = DB::table('users')->get();
    
    foreach($users as $user)
    {
        generate_user_access($user->id);
    }
}

function get_user_access($user_id = null)
{
    $permission = App\Models\Permission::where('user_id', '=', $user_id)->first();
    
    $data['elements']         = json_decode($permission->elements, true);
    $data['routes']           = json_decode($permission->routes, true);
    $data['ordered_elements'] = json_decode($permission->ordered_elements, true);
    
    return $data;
}

function get_locations($location_id = null)
{
    $locations = [];
    
    $result = \DB::table('locations')
                 ->whereNull('deleted_at');
    
    if(is_null($location_id))
    {
        $result = $result->whereNull('parent_id');
    }
    else
    {
        $result = $result->where('parent_id', '=', $location_id);
    }
    
    $result = $result->orderBy('created_at', 'desc')
                     ->get();
    
    foreach($result as $item)
    {
        if(!isset($locations[$item->variable]))
        {
            $children = \App\Models\Location::where('parent_id', '=', $item->id)->count();
            
            $locations[$item->variable] = [
                'id'          => $item->id,
                'parent_id'   => $item->parent_id,
                'description' => $item->description,
                'name'        => $item->name,
                'variable'    => $item->variable,
                'slug'        => $item->slug,
                'children'    => $children > 0 ? get_locations($item->id) : [],
            ];
        }
    }
    
    return $locations;
}

function get_equipments($equipment_id = null)
{
    $equipments = [];
    
    $result = \DB::table('sm_equipments')
                 ->whereNull('deleted_at');
    
    if(is_null($equipment_id))
    {
        $result = $result->whereNull('parent_id');
    }
    else
    {
        $result = $result->where('parent_id', '=', $equipment_id);
    }
    
    $result = $result->orderBy('created_at', 'desc')
                     ->get();
    
    foreach($result as $item)
    {
        if(!isset($equipments[$item->serial]))
        {
            $children = \App\Models\SistemaDeMantenimiento\Equipment::where('parent_id', '=', $item->id)->count();
            
            $equipments[$item->serial] = [
                'id'          => $item->id,
                'parent_id'   => $item->parent_id,
                'description' => $item->description,
                'serial'      => $item->serial,
                'slug'        => $item->slug,
                'children'    => $children > 0 ? get_equipments($item->id) : [],
            ];
        }
    }
    
    return $equipments;
}

function generate_users_access($users)
{
    foreach($users as $user)
    {
        generate_user_access($user->id);
    }
}

function secureUpdate($model)
{
    try
    {
        $model->save();
        
        return true;
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function secureSave($model)
{
    try
    {
        $model->save();
        
        return true;
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function secureDelete($model)
{
    try
    {
        $model->delete();
        
        return true;
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function get_user($request) : \App\Models\User
{
    $token    = $request->header('x-auth-token');
    $username = $request->header('x-auth-uid');
    
    return \App\Models\User::where(filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username', $username)
                           ->where('token', $token)
                           ->first();
}