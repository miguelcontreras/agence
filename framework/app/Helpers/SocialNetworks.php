<?php

function google_analytics($id)
{
    return "
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '".$id."', 'auto');
          ga('send', 'pageview');
        </script>
    ";
}

function fb_comments($max = 3, $width = '100%',$url = null)
{
    return '
        <div class="fb-comments" data-href="'.print_is_true(is_null($url) ? current_url() : $url).'" data-numposts="'.$max.'" data-width="'.$width.'"></div>
    ';
}

function fb_page($url = null, $show_posts = 'true', $tabs = null, $friend_faces = 'true', $hide_cover = 'false', $small_header = 'false')
{
    if(!is_null($tabs))
    {
        $tabs = 'data-tabs="'.$tabs.'"';
    }
    
    return '
        <div class="fb-page" data-href="'.print_is_true(is_null($url),url(),$url).'" data-small-header="'.$small_header.'" data-adapt-container-width="true" data-hide-cover="'.$hide_cover.'" data-show-facepile="'.$friend_faces.'" data-show-posts="'.$show_posts.'" '.$tabs.'></div>
    ';
}

/*
 * TODO: falta por hacer
 */

function iniciar_twitter()
{
    return "
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    ";
}

function compartir_twitter($sitio, $contenido, $usuario, $hashtag)
{
    if(!is_null($hashtag))
    {
        $hashtag = 'data-hashtags="'.$hashtag.'"';
    }
    
    $contenido = str_replace(array("'", '"'), '*', trim(mb_substr($contenido, 0, 70, 'UTF-8')).'...');
    
    return '
    <a href="https://twitter.com/share" class="twitter-share-button"{count} data-url="'.$sitio.'" data-text="'.$contenido.'" data-via="'.$usuario.'" '.$hashtag.'>Tweet</a>
    ';
}