<?php

function show_alert($type = 'message', $title = null, $message = 'Por favor escriba un mensaje', $dismiss = true)
{
    $icon = array(
        'success' => 'fa-check',
        'warning' => 'fa-warning',
        'info'    => 'fa-info',
        'danger'  => 'fa-ban',
    );
    ?>
    <div class="callout callout-<?= $type; ?> <?= ($dismiss) ? 'alert alert-dismissable' : ''; ?>">
        <?php
        if($dismiss)
        {
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php
        }
        
        if(!is_null($title))
        {
            ?><h4><i class="icon fa <?= $icon[$type]; ?>"></i> <?= $title; ?></h4><?php
        }
        
        if(is_array($message))
        {
            foreach($message as $text)
            {
                ?>
                <li><?= $text ?></li>
                <?php
            }
        }
        else
        {
            ?>
            <p><?= $message; ?></p>
            <?php
        }
        ?>
    </div>
    <?php
}

function sweet_alert($type = 'message', $message = 'Mensaje', $title = '', $persistent = false)
{
    switch($type)
    {
        case 'message':
            if($persistent)
            {
                alert()->message($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->message($message, $title);
            }
            break;
        case 'basic':
            if($persistent)
            {
                alert()->basic($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->basic($message, $title);
            }
            break;
        case 'info':
            if($persistent)
            {
                alert()->info($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->info($message, $title);
            }
            break;
        case 'success':
            if($persistent)
            {
                alert()->success($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->success($message, $title);
            }
            break;
        case 'error':
            if($persistent)
            {
                alert()->error($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->error($message, $title);
            }
            break;
        case 'warning':
            if($persistent)
            {
                alert()->warning($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->warning($message, $title);
            }
            break;
        default:
            if($persistent)
            {
                alert()->message($message, $title)->persistent('Cerrar');
            }
            else
            {
                alert()->message($message, $title);
            }
            break;
    }
}

function well($title, $message)
{
    ?>
    <div class="well registro-vacio text-center">
        <h1><?= $title; ?></h1>
        <p><?= $message; ?></p>
    </div>
    <?php
}

function validator_error($validator)
{
    $errors = [];
    $fields = $validator->errors()->getMessages();
    
    foreach($fields as $field => $messages){
        foreach($messages as $message){
            array_push($errors,$message);
        }
    }
    
    return [
        'msg'   => '¡Wrong data!',
        'flash' => [
            'title'   => (count($errors) == 1) ? 'El formulario presenta el siguiente inconveniente:' : 'El formulario presenta los siguientes inconvenientes:',
            'message' => $errors,
        ]
    ];
}

function save_error($title_flash = '¡Ha ocurrido un problema guardando la información!', $title_swal = '¡Error!')
{
    return [
        'msg'   => '¡Error!',
        'flash' => [
            'title'   => $title_flash,
            'message' => null,
        ],
        'swal'  => [
            'title'   => $title_swal,
            'message' => null,
        ],
    ];
}

function save_partial_error($title_flash,$title_swal,$slug = null)
{
    return [
        'msg'   => '¡Error!',
        'flash' => [
            'title'   => $title_flash,
            'message' => null,
        ],
        'swal'  => [
            'title'   => $title_swal,
            'message' => null,
        ],
        'slug' => $slug
    ];
}

function delete_error($title_flash = '¡Ha ocurrido un problema eliminando la información!')
{
    return [
        'msg'   => '¡Error!',
        'flash' => [
            'title'   => $title_flash,
            'message' => null,
        ],
        'swal'  => [
            'title'   => '¡Error!',
            'message' => null,
        ],
    ];
}

function not_found($title = 'La información',$action = 'buscar')
{
    return [
        'msg'   => '¡Not Found!',
        'flash' => [
            'title'   => '¡'.$title.' ha '.$action.' no existe!',
            'message' => null,
        ],
        'swal'  => [
            'title'   => '¡No existe!',
            'message' => null,
        ],
    ];
}

function no_change()
{
    return [
        'msg'   => '¡No change!',
        'flash' => [
            'title'   => '¡No se produjo ningún cambio!',
            'message' => null,
        ],
        'swal'  => [
            'title'   => '¡Sin Cambios!',
            'message' => null,
        ],
    ];
}

function save_ok($title_flash = '¡Se ha guardado la información correctamente!', $title_swal = '¡Guardado!',$data = null)
{
    return [
        'msg'   => '¡Success!',
        'flash' => [
            'title'   => $title_flash,
            'message' => null,
        ],
        'swal'  => [
            'title'   => $title_swal,
            'message' => null,
        ],
        'data' => $data
    ];
}

function delete_ok($title_flash = '¡Se ha eliminado la información correctamente!', $title_swal = '¡Eliminado!')
{
    return [
        'msg'   => '¡Success!',
        'flash' => [
            'title'   => $title_flash,
            'message' => null,
        ],
        'swal'  => [
            'title'   => $title_swal,
            'message' => null,
        ],
    ];
}