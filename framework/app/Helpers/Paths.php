<?php

function image_url($file = '', $only_url = false)
{
    if(!preg_match('/(https?:\/\/)/', $file))//is not a url
    {
        if($only_url = false && strpos(strtolower($file), 'default.png') === false && (!file_exists(assets_path('images/'.$file)) || !is_file(assets_path('images/'.$file))))
        {
            $file_parts = explode('/', $file);
            
            unset($file_parts[count($file_parts) - 1]);
            
            $file = trim(implode("/", $file_parts), '/').'/default.png';
        }
        
        return public_path('assets/images/'.$file);
    }
    else
    {
        return $file;
    }
}

function css_url_file($file = "", $tag = true)
{
    return ($tag ? '<link rel="stylesheet" type="text/css" href="' : '').public_path('assets/css/'.$file).($tag ? '" />' : '');
}

function js_url_file($file = '', $tag = true)
{
    return ($tag ? '<script type="text/javascript" src="' : '').public_path('assets/js/'.$file).($tag ? '"></script>' : '');
}

function plugins_css_url_file($file = '', $tag = true)
{
    return ($tag ? '<link rel="stylesheet" type="text/css" href="' : '').public_path('assets/plugins/'.$file).($tag ? '" />' : '');
}

function plugins_js_url_file($file = '', $tag = true)
{
    return ($tag ? '<script type="text/javascript" src="' : '').public_path('assets/plugins/'.$file).($tag ? '"></script>' : '');
}

function reports_url($file = '', $tag = true)
{
    return public_path('assets/reports/'.$file);
}

function assets_path($file = '')
{
    return base_path('../init/assets/'.$file);
}

function page_url($url)
{
    return '/#/'.$url;
}

function panel_url($url)
{
    return '/panel'.page_url($url);
}

