<?php

function time_diff($before, $type = 'minutes', $after = null)
{
    $after = (is_null($after)) ? time() : strtotime($after);
    
    switch($type)
    {
        case 'days':
            return intval(($after - strtotime($before))/(60*60*24));
            break;
        case 'minutes':
            return intval(($after - strtotime($before))/60);
            break;
        case 'seconds':
            return intval($after - strtotime($before));
            break;
        default:
            return intval(($after - strtotime($before))/60);
            break;
    }
}

function custom_date_format($date = null, $day = true, $month = true, $year = true, $separator = '/')
{
    if(is_null($date))
    {
        $date = date('Y-m-d');
    }
    else
    {
        $date = explode(' ', $date)[0];
    }
    
    $months = site_config()['months'];
    
    $date_parts = explode('-', $date);
    
    $formatted_date = '';
    
    if($day)
    {
        $formatted_date .= $date_parts[2].$separator;
    }
    if($month)
    {
        $formatted_date .= $months[$date_parts[1]].$separator;
    }
    if($year)
    {
        $formatted_date .= $date_parts[0];
    }
    
    return $formatted_date;
}

function custom_hour_format($hour, $show_seconds = false)
{
    $hour_parts = explode(':', $hour);
    
    $hour    = intval($hour_parts[0]);
    $minutes = $hour_parts[1];
    $seconds = $hour_parts[2];
    
    if($hour > 12)
    {
        $hour -= 12;
        
        $meridiem = 'PM';
    }
    else
    {
        if($hour == 12)
        {
            $meridiem = 'PM';
        }
        else
        {
            if($hour == 0)
            {
                $hour = 12;
            }
            
            $meridiem = 'AM';
        }
    }
    
    return print_is_true($hour < 10, '0').$hour.':'.$minutes.print_is_true($show_seconds, ':'.$seconds.' ').$meridiem;
}

function reverse_date($date, $normal = true, $separator = '-') // 04 Enero 2016
{
    if(!$normal)
    {
        $date_day   = explode(' ', $date)[0];
        $date_parts = explode($separator, $date_day);
        
        $year  = $date_parts[2];
        $month = (array_search($date_parts[1], site_config()['months'])) ? array_search($date_parts[1], site_config()['months']) : $date_parts[1];
        $day   = $date_parts[0];
        
        $date_format = $year.'-'.$month.'-'.$day;
        
        if(isset(explode(' ', $date)[1]))
        {
            $date_format .= ' '.explode(' ', $date)[1];
        }
    }
    else
    {
        $date_format = $date;
    }
    
    return strtotime($date_format);
}

function reverse_hour($hour,$seconds = false)
{
    $hour = str_replace(' ','',$hour);
    $meridiem = strtoupper(substr($hour,-2,2));
    $hour = substr($hour,0,-2);
    
    $hour_parts = explode(':', $hour);
    
    $hour    = $hour_parts[0];
    $minute  = $hour_parts[1];
    $seconds = (isset($hour_parts[2]) && $seconds) ? $hour_parts[2] : '00';
    
    if($meridiem == 'PM')
    {
        if($hour != 12)
        {
            $hour += 12;
        }
    }
    else
    {
        if($hour == 12)
        {
            $hour = '00';
        }
    }
    
    return $hour.':'.$minute.':'.$seconds;
}

function first_day_of_week($date = null)
{
    if(is_null($date))
    {
        $time = strtotime(date('Y-m-d 00:00:00'));
    }
    else
    {
        $time = strtotime($date);
    }
    
    $year = date('Y',$time);
    $week = date('W',$time);
    
    return date('Y-m-d 00:00:00', strtotime($year . 'W' . str_pad($week , 2, '0', STR_PAD_LEFT)));
}

function week($date = null)
{
    $start = first_day_of_week($date);
    
    return [
        $start,
        date('Y-m-d 00:00:00', strtotime($start.' +1 day')),
        date('Y-m-d 00:00:00', strtotime($start.' +2 day')),
        date('Y-m-d 00:00:00', strtotime($start.' +3 day')),
        date('Y-m-d 00:00:00', strtotime($start.' +4 day')),
        date('Y-m-d 00:00:00', strtotime($start.' +5 day')),
        date('Y-m-d 00:00:00', strtotime($start.' +6 day')),
    ];
}

function last_day_of_de_month($month = null,$year = null)
{
    $month = is_null($month) ? date('m') : $month;
    $year = is_null($year) ? date('Y') : $year;
    
    $next_month = date('Y-m-d', strtotime($year.'-'.$month."-01 + 1 month"));
    
    return date('d', strtotime($next_month." - 1 day"));
}

/*
 * TODO: por hacer
 */

function long_date($date = null) // Lunes, 04 de Enero del 2016 a las 10:57 am
{
    //code here
}

function short_date($date = null) // 04 Enero 2016
{
    //code here
}