<?php

function has_something($value, $length = 2)
{
    return !is_null($value) && $value !== 'null' && (!is_array($value) && strlen($value) > $length || is_array($value) && count($value) > 0);
}

function is_true($value)
{
    return $value === true;
}

function print_is_true($eval, $value, $value_if_false = '')
{
    return (is_true($eval)) ? $value : $value_if_false;
}

function validate($data, $rules)
{
    return Validator::make($data, $rules);
}