<?php

function sendMailQueue($data)
{
    $email = new \App\Models\EmailQueue();
    
    if(isset($data['to']))
    {
        $email->to = json_encode($data['to']);
    }
    
    if(isset($data['from']))
    {
        $email->from = json_encode($data['from']);
    }
    
    if(isset($data['copy']))
    {
        $email->copy = json_encode($data['copy']);
    }
    
    if(isset($data['file']))
    {
        $email->file = json_encode($data['file']);
    }
    
    if(isset($data['date']))
    {
        $email->date = $data['date'];
    }
    else
    {
        $email->date = date('Y-m-d H:i:s');
    }
    
    if(isset($data['template']))
    {
        $email->template = $data['template'];
    }
    
    $email->data    = json_encode($data['data']);
    $email->subject = $data['subject'];
    
    return secureSave($email);
}

function sendMail($data)
{
    try
    {
        $config = \App\Models\SystemConfig::where('code', '=', 'email')->first();
        $config = json_decode($config->value, true);
        
        Config::set('mail.host', $config['host']);
        Config::set('mail.port', $config['port']);
        Config::set('mail.from.address', $config['email']);
        Config::set('mail.from.name', env('MAIL_FROM_NAME'));
        Config::set('mail.username', $config['email']);
        Config::set('mail.password', $config['password']);
        
        #si se está en modo de pruebas, entonces todos los mails que salgan al correo de pruebas
        if(env('APP_DEBUG'))
        {
            $data['to'] = env('MAIL_DEBUG');
        }
        
        ( new Illuminate\Mail\MailServiceProvider(app()) )->register();
        
        $template = isset($data['template']) ? $data['template'] : 'mail.general';
        
        \Mail::send($template, isset($data['data']) ? $data['data'] : $data, function($m) use ($data, $config){
            
            if(isset($data['from']))
            {
                $m->from($data['from']['address'], $data['from']['name']);
            }
            else
            {
                $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            }
            
            if(isset($data['to']))
            {
                if(is_array($data['to']))
                {
                    foreach($data['to'] as $to)
                    {
                        $m->to($to);
                    }
                }
                else
                {
                    $m->to($data['to']);
                }
            }
            else
            {
                $m->to($config['email']);
            }
            
            if(isset($data['copy']))
            {
                if(is_array($data['copy']))
                {
                    foreach($data['copy'] as $copy)
                    {
                        $m->bcc($copy);
                    }
                }
                else
                {
                    $m->bcc($data['copy']);
                }
            }
            
            $m->subject($data['subject']);
            
            if(isset($data['file']) && !is_null($data['file']) && file_exists($data['file']['path']) && is_file($data['file']['path']))
            {
                $data_mail = [];
                
                if(isset($data['file']['filename']) && !is_null($data['file']['filename']))
                {
                    $data_mail['as'] = $data['file']['filename'];
                }
                
                $m->attach($data['file']['path'], $data_mail);
            }
        });
        
        return true;
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}