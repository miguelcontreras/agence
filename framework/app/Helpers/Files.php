<?php

function permitted_extension($extension, $permitted_extensions)
{
    if(in_array(strtolower($extension), $permitted_extensions))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function delete_file($file_path, $file_name)
{
    $file_path = trim($file_path, '/').'/';
    
    if(!in_array($file_name, [
            'default.png',
            'index.blade.php',
            'male-default.png',
            'female-default.png',
            'index.blade.php',
            'index.html',
        ]) && file_exists(assets_path($file_path.$file_name))
    )
    {
        try
        {
            return unlink(assets_path($file_path.$file_name));
        }
        catch(Exception $e)
        {
            return false;
        }
    }
    else
    {
        return true;
    }
}

function save_file($file, $file_path, $file_name)
{
    try
    {
        if(!is_null($file))
        {
            $file_path = assets_path($file_path.'/');
            
            if($file->move($file_path, $file_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function type_of_file($extension)
{
    switch($extension)
    {
        case 'png':
        case 'jpg':
        case 'jpeg':
            return 'image';
            break;
        case 'pdf':
            return 'pdf';
            break;
        case 'doc':
        case 'docx':
            return 'word';
            break;
        case 'xls':
        case 'xlsx':
        case 'csv':
            return 'excel';
            break;
        case 'ppt':
        case 'pptx':
            return 'powerpoint';
            break;
        default:
            return 'unknown';
            break;
    }
}

function resize_image($image, $file_path, $file_name, $width, $height = null)
{
    try
    {
        if(is_null($height))
        {
            $new_image = \Intervention\Image\ImageManagerStatic::make($image)
                                                               ->resize($width, null, function($constraint){
                                                                   $constraint->aspectRatio();
                                                               });
        }
        else
        {
            $new_image = Intervention\Image\ImageManagerStatic::make($image)->resize($width, $height);
        }
        
        $file_path = assets_path($file_path.'/');
        
        if($new_image->save($file_path.$file_name))
        {
            return true;
        }
        else
        {
            return [
                'msg'   => '¡Error!',
                'flash' => [
                    'title'   => '¡No se pudo guardar la imagen!',
                    'message' => null,
                ],
                'swal'  => [
                    'title'   => '!Imagen no guardada¡',
                    'message' => null,
                ],
            ];
        }
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return [
            'msg'   => '¡Error!',
            'flash' => [
                'title'   => '¡No se pudo guardar la imagen!',
                'message' => null,
            ],
            'swal'  => [
                'title'   => '!Imagen no guardada¡',
                'message' => null,
            ],
        ];
    }
}

function pdf_to_image($path_pdf, $path_image)
{
    try
    {
        $image = new Imagick();
        $image->readImage(assets_path($path_pdf).'[0]');
        $image->setImageFormat('jpg');
        $image->writeImage(assets_path($path_image));
        
        return true;
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function get_preview($filename, $type, $path)
{
    switch($type)
    {
        case 'word':
        case 'excel':
        case 'powerpoint':
            return 'images/extensions/'.$type.'.png';
            break;
        case 'pdf':
            $filename_without_extension = get_filename($filename);
            
            if(file_exists(assets_path($path.'/'.$filename_without_extension.'.jpg')))
            {
                return $path.'/'.$filename_without_extension.'.jpg';
            }
            else
            {
                return 'images/extensions/'.$type.'.png';
            }
            break;
        case 'image':
            return $path.'/'.$filename;
            break;
        default:
            return $path.'/default.png';
            break;
    }
}

function get_filename($filename)
{
    $parts = explode('.', $filename);
    
    $text = '';
    
    for($i = 0; $i < count($parts) - 1; $i++)
    {
        $text .= ( $i != 0 ? '.' : '' ).$parts[$i];
    }
    
    return $text;
}

function base64_to_image($filename, $extension, $path, $base64_string)
{
    try
    {
        $final_filename = $filename.'.'.$extension;
        $count          = 1;
        
        while(file_exists(assets_path($path).$final_filename))
        {
            $count++;
            
            $final_filename = $filename.' ('.$count.')'.'.'.$extension;
        }
        
        // open the output file for writing
        $ifp = fopen(assets_path($path).$final_filename, 'wb');
        
        // we could add validation here with ensuring count( $data ) > 1
        $result = fwrite($ifp, base64_decode($base64_string));
        // clean up the file resource
        fclose($ifp);
        
        return $result ? $final_filename : 'default.png';
    }
    catch(Exception $e)
    {
        return 'default.png';
    }
}

function unzip($file, $path)
{
    try
    {
        // Check if webserver supports unzipping.
        if(!class_exists('ZipArchive'))
        {
            return false;
        }
        
        $zip = new ZipArchive;
        // Check if archive is readable.
        if($zip->open($file) === true)
        {
            // Check if destination is writable
            if(is_writeable(assets_path($path)))
            {
                $zip->extractTo(assets_path($path));
                $zip->close();
                
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    catch(Exception $e)
    {
        if(env('APP_DEBUG'))
        {
            dd($e->getMessage());
        }
        
        return false;
    }
}

function get_image($image, $element)
{
    if($image != 'default.png')
    {
        $filename  = explode('.', $image);
        $image     = $filename[0];
        $extension = $filename[count($filename) - 1];
        
        return $image.$element.'.'.$extension;
    }
    else
    {
        return 'default.png';
    }
}

function get_extension($file)
{
    $extension = explode('.', $file);
    $extension = $extension[count($extension) - 1];
    
    return strtolower($extension);
}

function get_dir_by_extension($extension)
{
    if($extension == 'doc' || $extension == 'docx')
    {
        $dir = 'word';
    }
    else
    {
        if($extension == 'rar' || $extension == 'zip')
        {
            $dir = 'compressed';
        }
        else
        {
            if($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv')
            {
                $dir = 'excel';
            }
            else
            {
                if($extension == 'pptx')
                {
                    $dir = 'slide';
                }
                else
                {
                    if($extension == 'txt')
                    {
                        $dir = 'txt';
                    }
                    else
                    {
                        if($extension == 'pdf')
                        {
                            $dir = 'pdf';
                        }
                        else
                        {
                            if($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif')
                            {
                                $dir = 'image';
                            }
                            else
                            {
                                $dir = 'unknown';
                            }
                        }
                    }
                }
            }
        }
    }
    
    return $dir;
}

function custom_is_file($url)
{
    $is_file = false;
    
    $regex_file_extensions = [
        'tar\.gz',
        '\.css',
        '\.js',
        '\.txt',
        '\.doc',
        '\.docx',
        '\.pdf',
        '\.xls',
        '\.xlsx',
        '\.ppt',
        '\.pptx',
        '\.png',
        '\.jpeg',
        '\.jpg',
        '\.gif',
        '\.mp4',
        '\.mp3',
        '\.ico',
        '\.rar',
        '\.zip',
    ];
    
    $url = strtolower($url);
    
    foreach($regex_file_extensions as $regex)
    {
        if(preg_match('/'.$regex.'/', $url))
        {
            $is_file = true;
            break;
        }
    }
    
    return $is_file;
}

/**
 * TODO: faltan por realizarse
 */

function water_drop($file_path, $text = null, $size = '24')
{
    //code here
}