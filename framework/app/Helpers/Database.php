<?php

function get_all_tables()
{
    $tables = [];
    
    /*$result = DB::select('SHOW TABLES');*/
    
    $result = DB::getDoctrineSchemaManager()->listTableNames();
    $prefix = getDBConfig();
    
    foreach($result as $table)
    {
        if(strlen($prefix) > 0)
        {
            $parts = explode($prefix, $table);//$table->Tables_in_robot_project
            if(count($parts) > 1 && $parts[1] != 'migrations')
            {
                array_push($tables, $parts[1]);
            }
        }
        else
        {
            if($table != 'migrations')
            {
                array_push($tables, $table);
            }
        }
    }
    
    return $tables;
}

function enable_query_log()
{
    \DB::enableQueryLog();
}

function last_query()
{
    $queries = \DB::getQueryLog();
    
    $last_query = end($queries);
    
    foreach($last_query['bindings'] as $val)
    {
        $last_query['query'] = preg_replace('/\?/', "'{$val}'", $last_query['query'], 1);
    }
    
    dd($last_query['query']);
}

function all_queries()
{
    $queries = \DB::getQueryLog();
    $data = [];
    
    foreach($queries as $query)
    {
        $result = $query;
        
        foreach($result['bindings'] as $val)
        {
            $result['query'] = preg_replace('/\?/', "'{$val}'", $result['query'], 1);
        }
        
        array_push($data,$result['query']);
    }
    
    dd($data);
}

function getDBConfig($config = 'prefix')
{
    return \DB::connection()->getConfig($config);
}

function getRealQuery($query)
{
    $real_query = $query->toSql();
    
    foreach($query->getBindings() as $val)
    {
        $real_query = preg_replace('/\?/', "'{$val}'", $real_query, 1);
    }
    
    return $real_query;
}