<?php
function site_config()
{
    $config["site_name"]  = env('APP_NAME', 'Agence Test');
    $config["created_at"] = "2019";
    
    $config["maker"]       = "Miguel Contreras";
    $config["maker_link"]  = "#";
    $config['false_maker'] = null;
    
    $config["panel_name"]            = 'PANEL';
    $config["version"]               = 'Version 1.0';
    
    $config["months"] = [
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro',
    ];
    
    $config["days"] = [
        '1' => 'Lun',
        '2' => 'Mar',
        '3' => 'Mié',
        '4' => 'Jue',
        '5' => 'Vie',
        '6' => 'Sáb',
        '7' => 'Dom',
    ];
    
    return $config;
}
