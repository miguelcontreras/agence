<?php

function muted_text($text)
{
    return '<em class="text-muted">'.$text.'</em>';
}

function btn_create($route, $type = 'success', $icon = 'glyphicon glyphicon-plus-sign')
{
    return '<a href="'.$route.'" class="btn btn-'.$type.' pull-right" style="margin-top:-10px;"><span class="'.$icon.'"></span><p class="hidden-xs" style="display: inline"> Agregar</p></a>';
}

function btn_edit($route, $type = 'primary', $icon = 'glyphicon glyphicon-pencil')
{
    return btn_link($route, 'Editar', $type, $icon);
}

function btn_destroy($id, $type = 'danger', $icon = 'glyphicon glyphicon-trash',$text = 'Eliminar')
{
    return '<a href="#" id="delete_'.$id.'" class="btn btn-'.$type.' confirmation_delete_modal" title="'.$text.'"><span class="'.$icon.'" aria-hidden="true"></span></a>';
}

function btn_link($route, $tooltip, $type = 'default', $icon = 'glyphicon glyphicon-th-list', $text = null, $class = '',$id = null)
{
    return '<a href="'.$route.'" class="btn btn-'.$type.' '.$class.'" '.print_is_true(!is_null($id),'id="'.$id.'"').' title="'.$tooltip.'"><span class="'.$icon.'" aria-hidden="true"></span>'.print_is_true(!is_null($text), ' '.$text, '').'</a>';
}

/*
 * TODO: falta por hacer
 */

function generate_captcha()
{
    //code here
}