<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::pattern('id', '[0-9]+');
        Route::pattern('username', '^(?!agregar)[0-9a-zA-Z_-]+$');
        Route::pattern('slug_parent', '^(?!agregar)[0-9a-zA-Z_-]+$');
        Route::pattern('slug', '[0-9a-zA-Z_-]+');
        Route::pattern('parent', '[0-9a-zA-Z_-]+');
        
        parent::boot();
    }
    
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        /*$this->mapApiRoutes();*/
        $this->mapWebRoutes();
        $this->mapCustomApiRoutes();
    }
    
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }
    
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    
    protected function mapCustomApiRoutes()
    {
        $routes_path = app_path('Http/Routes').DIRECTORY_SEPARATOR;
    
        foreach(scandir($routes_path) as $file)
        {
            if(is_file($routes_path.$file))
            {
                Route::prefix('api')
                     ->middleware('api')
                     ->namespace($this->namespace)
                     ->group($routes_path.$file);
            }
            else
            {
                if(!in_array($file, [ '.', '..' ]) && is_dir($routes_path.$file))
                {
                    foreach(scandir($routes_path.$file) as $subfile)
                    {
                        if(is_file($routes_path.$file.DIRECTORY_SEPARATOR.$subfile))
                        {
                            Route::prefix('api')
                                 ->middleware('api')
                                 ->namespace($this->namespace)
                                 ->group($routes_path.$file.DIRECTORY_SEPARATOR.$subfile);
                        }
                    }
                }
            }
        }
    }
}
