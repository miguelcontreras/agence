<?php

namespace App\Http\Controllers\Backend;

use App\Models\Invoice;
use App\Models\Salary;
use App\Models\ServiceOrder;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ConsultantController extends Controller
{
    public function report(Request $request)
    {
        $service_order_table = ServiceOrder::getTableName();
        $invoice_table       = Invoice::getTableName();
        $salary_table        = Salary::getTableName();
        
        $consultants = User::with([
            /*podria optimizarse dejando que php haga todo este trabajo y no la bd*/
                                      'serviceOrders' => function($query) use ($service_order_table, $invoice_table, $salary_table, $request){
                                          return $query->select([
                                                                    \DB::raw('SUM('.$invoice_table.'.valor * (1 - '.$invoice_table.'.total_imp_inc/100)) as net_income'),
                                                                    $salary_table.'.brut_salario as fixed_cost',
                                                                    \DB::raw('SUM(('.$invoice_table.'.valor * (1 - '.$invoice_table.'.total_imp_inc/100))*'.$invoice_table.'.comissao_cn/100) as commission'),
                                                                    \DB::raw('MONTH('.$invoice_table.'.data_emissao) as month'),
                                                                    \DB::raw('YEAR('.$invoice_table.'.data_emissao) as year'),
                                                                    $service_order_table.'.co_usuario',
                                                                ])
                                                       ->join($invoice_table, $invoice_table.'.co_os', '=', $service_order_table.'.co_os')
                                                       ->join($salary_table, $salary_table.'.co_usuario', '=', $service_order_table.'.co_usuario')
                                                       ->whereBetween($invoice_table.'.data_emissao', [ $request->range['from'], $request->range['to'] ])
                                                       ->groupBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy($salary_table.'.brut_salario')
                                                       ->groupBy($service_order_table.'.co_usuario')
                                                       ->orderBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->orderBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'));
                                      },
                                  ])
                           ->whereIn('co_usuario', $request->consultants)
                           ->get();
        
        return response()->json([
                                    'success' => true,
                                    'html'    => view('frontend.home.report', [
                                        'consultants' => $consultants,
                                    ])->render(),
                                ], 200);
    }
    
    public function graph(Request $request)
    {
        $service_order_table = ServiceOrder::getTableName();
        $invoice_table       = Invoice::getTableName();
        
        $consultants = explode(',', $request->consultants);
        $range       = [
            'from' => $consultants[count($consultants) - 2],
            'to'   => $consultants[count($consultants) - 1],
        ];
        
        #resolvi de ésta forma puesto que por por el formato en cómo se escribió la llamada en esta funcion, no me dejó colocar más variables $_GET
        unset($consultants[count($consultants) - 1]);
        unset($consultants[count($consultants) - 1]);
        
        $consultants = User::with([
                                      'serviceOrders' => function($query) use ($service_order_table, $invoice_table, $range){
                                          return $query->select([
                                                                    \DB::raw('SUM('.$invoice_table.'.valor * (1 - '.$invoice_table.'.total_imp_inc/100)) as net_income'),
                                                                    \DB::raw('MONTH('.$invoice_table.'.data_emissao) as month'),
                                                                    \DB::raw('YEAR('.$invoice_table.'.data_emissao) as year'),
                                                                    $service_order_table.'.co_usuario',
                                                                ])
                                                       ->join($invoice_table, $invoice_table.'.co_os', '=', $service_order_table.'.co_os')
                                                       ->whereBetween($invoice_table.'.data_emissao', [ $range['from'], $range['to'] ])
                                                       ->groupBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy($service_order_table.'.co_usuario')
                                                       ->orderBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->orderBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'));
                                      },
                                      'salary',
                                  ])
                           ->whereIn('co_usuario', $consultants)
                           ->get();
        
        return response()->view('frontend.home.graph', [
            'consultants'    => $consultants,
            'month_from'     => intval(date('m', strtotime($range['from']))),
            'month_to'       => intval(date('m', strtotime($range['to']))),
            'year_from'      => date('Y', strtotime($range['from'])),
            'year_to'        => date('Y', strtotime($range['to'])),
            'avg_fixed_cost' => $consultants->map(function($consultant){/*promedio del costo fijo sumando los salarios brutos (si tiene)*/
                return is_null($consultant->salary) ? 0 : $consultant->salary->brut_salario;
            })->avg(),
        ])->header('Content-Type', 'text/xml');
    }
    
    public function pizza(Request $request)
    {
        $service_order_table = ServiceOrder::getTableName();
        $invoice_table       = Invoice::getTableName();
    
        $consultants = explode(',', $request->consultants);
        $range       = [
            'from' => $consultants[count($consultants) - 2],
            'to'   => $consultants[count($consultants) - 1],
        ];
    
        #resolvi de ésta forma puesto que por por el formato en cómo se escribió la llamada en esta funcion, no me dejó colocar más variables $_GET
        unset($consultants[count($consultants) - 1]);
        unset($consultants[count($consultants) - 1]);
    
        $consultants = User::with([
            /*esta parte podria reulizarse con graph*/
                                      'serviceOrders' => function($query) use ($service_order_table, $invoice_table, $range){
                                          return $query->select([
                                                                    \DB::raw('SUM('.$invoice_table.'.valor * (1 - '.$invoice_table.'.total_imp_inc/100)) as net_income'),
                                                                    \DB::raw('MONTH('.$invoice_table.'.data_emissao) as month'),
                                                                    \DB::raw('YEAR('.$invoice_table.'.data_emissao) as year'),
                                                                    $service_order_table.'.co_usuario',
                                                                ])
                                                       ->join($invoice_table, $invoice_table.'.co_os', '=', $service_order_table.'.co_os')
                                                       ->whereBetween($invoice_table.'.data_emissao', [ $range['from'], $range['to'] ])
                                                       ->groupBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->groupBy($service_order_table.'.co_usuario')
                                                       ->orderBy(\DB::raw('YEAR('.$invoice_table.'.data_emissao)'))
                                                       ->orderBy(\DB::raw('MONTH('.$invoice_table.'.data_emissao)'));
                                      },
                                  ])
                           ->whereIn('co_usuario', $consultants)
                           ->get();
    
        return response()->view('frontend.home.pizza', [
            'consultants'    => $consultants,
            'total' => $consultants->map(function($consultant){/*se suman todos las ganancias netas de un consultor y luego se suman la de todos los consultores*/
                return $consultant->serviceOrders->sum('net_income');
            })->sum(),
        ])->header('Content-Type', 'text/xml');
    }
}
