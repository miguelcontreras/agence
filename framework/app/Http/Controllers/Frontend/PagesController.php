<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Mail\MailServiceProvider;

class PagesController extends Controller
{
    public function __construct()
    {
    
    }
    
    public function index()
    {
        #podria hacer uso del join para que solamente haga una consulta interna, pero para efectos de simplicidad utilicé el whereHas
        $consultants = User::whereHas('permission', function($query){
            return $query->where('co_sistema', '=', 1)
                         ->where('in_ativo', '=', 'S')
                         ->whereIn('co_tipo_usuario', [ 0, 1, 2 ]);
        })->get();
        
        return view('frontend.home.index',
                    [
                        'page_title'  => 'CAOL - Controle de Atividades Online',
                        'consultants' => $consultants,
                    ]);
    }
}
