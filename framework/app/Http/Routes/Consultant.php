<?php

namespace App\Http\Routes;

use Route;

/**
 * Rutas de Consultantes
 */

Route::group([
                 'prefix' => 'consultant',
             ],
    function(){
        Route::post('report', [
            'as'   => 'consultant.report',
            'uses' => 'Backend\ConsultantController@report',
        ]);
    
        Route::post('pizza', [
            'as'   => 'consultant.pizza',
            'uses' => 'Backend\ConsultantController@pizza',
        ]);
    }
);

