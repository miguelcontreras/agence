<?php

return [
    'full'   => [
        'category'         => [
            'singular' => 'categoría',
            'plural'   => 'categorías',
        ],
        'article'          => [
            'singular' => 'artículo',
            'plural'   => 'artículos',
        ],
        'unit of measure'  => [
            'singular' => 'unidad de medida',
            'plural'   => 'unidades de medida',
        ],
        'classification'   => [
            'singular' => 'clasificación',
            'plural'   => 'clasificaciones',
        ],
        'brand'            => [
            'singular' => 'marca',
            'plural'   => 'marcas',
        ],
        'model'            => [
            'singular' => 'modelo',
            'plural'   => 'modelos',
        ],
        'state'            => [
            'singular' => 'estado',
            'plural'   => 'estados',
        ],
        'city'             => [
            'singular' => 'ciudad',
            'plural'   => 'ciudades',
        ],
        'client'           => [
            'singular' => 'cliente',
            'plural'   => 'clientes',
        ],
        'location'         => [
            'singular' => 'ubicación',
            'plural'   => 'ubicaciones',
        ],
        'product'          => [
            'singular' => 'producto',
            'plural'   => 'productos',
        ],
        'entry order'      => [
            'singular' => 'órden de entrada',
            'plural'   => 'órdenes de entrada',
        ],
        'entry'            => [
            'singular' => 'entrada',
            'plural'   => 'entradas',
        ],
        'out order'        => [
            'singular' => 'órden de salida',
            'plural'   => 'órdenes de salida',
        ],
        'out'              => [
            'singular' => 'salida',
            'plural'   => 'salidas',
        ],
        'devolution order' => [
            'singular' => 'órden de devolución',
            'plural'   => 'órdenes de devolución',
        ],
        'devolution'       => [
            'singular' => 'devolución',
            'plural'   => 'devoluciones',
        ],
        'main_operation'   => [
            'singular' => 'operación principal',
            'plural'   => 'operaciones principales',
        ],
        'order product'    => [
            'singular' => 'producto de la orden',
            'plural'   => 'productos de la orden',
        ],
        'report'           => [
            'singular' => 'reporte',
            'plural'   => 'reportes',
        ],
        'inventory'        => [
            'singular' => 'inventario',
            'plural'   => 'inventario',
        ],
        'pharmacy'         => [
            'singular' => 'farmacia',
            'plural'   => 'farmacias',
        ],
        'module'           => [
            'singular' => 'módulo',
            'plural'   => 'Módulos',
        ],
        'submodule'        => [
            'singular' => 'submodulo',
            'plural'   => 'submodulos',
        ],
        'route'            => [
            'singular' => 'ruta',
            'plural'   => 'rutas',
        ],
        'role'             => [
            'singular' => 'rol',
            'plural'   => 'roles',
        ],
        'permission'       => [
            'singular' => 'permiso',
            'plural'   => 'permisos',
        ],
        'product_story'    => [
            'singular' => 'historia del producto',
            'plural'   => 'historias de los productos',
        ],
        'alert'            => [
            'singular' => 'alerta',
            'plural'   => 'alertas',
        ],
        'phase'            => [
            'singular' => 'fase',
            'plural'   => 'fases',
        ],
        'dispositive'      => [
            'singular' => 'dispositivo',
            'plural'   => 'dispositivos',
        ],
    ],
    'plural' => [
        'users'                  => 'Usuarios',
        'pharmacies'             => 'Farmacias',
        'visits'                 => 'Visitas',
        'administrative_panel'   => 'Panel de Administración',
        'logo'                   => 'Logo',
        'social_networks'        => 'Redes Sociales',
        'contact'                => 'Contacto',
        'summary'                => 'Resumen',
        'referrers'              => 'Referidos',
        'units_of_measure'       => 'Unidades de Medida',
        'products_configuration' => 'Configuración de Productos',
        'classifications'        => 'Clasificaciones',
        'brands'                 => 'Marcas',
        'models'                 => 'Modelos',
        'states'                 => 'Estados',
        'cities'                 => 'Ciudades',
        'clients'                => 'Clientes',
        'locations'              => 'Ubicaciones',
        'products'               => 'Productos',
        'orders'                 => 'Órdenes',
        'entries'                => 'Entradas',
        'main_operations'        => 'Operaciones Principales',
        'entry_orders'           => 'Órdenes de Entrada',
        'order_products'         => 'Productos de la Orden',
        'outs'                   => 'Salidas',
        'out_orders'             => 'Órdenes de Salida',
        'devolutions'            => 'Devoluciones',
        'devolution_orders'      => 'Órdenes de Devolución',
        'reports'                => 'Reportes',
        'inventory'              => 'Inventario',
        'recent_activities'      => 'Actividades Recientes',
        'site_config'            => 'Configuración del Sitio',
        'modules'                => 'Módulos',
        'submodules'             => 'SubModulos',
        'routes'                 => 'Rutas',
        'sync_up_access'         => 'Sincronizar Acceso',
        'roles'                  => 'Roles',
        'permissions'            => 'Permisos',
        'product_stories'        => 'Historias de los Productos',
        'search_products'        => 'Busqueda de Productos',
        'alerts'                 => 'Alertas',
        'robot'                  => 'Robot',
        'logs'                   => 'Logs',
        'robot_config'           => 'Robot',
        'top'                    => 'TOP',
        'top_products_sought'    => 'Top Productos Buscados',
        'dashboard'              => 'Resumen',
        'phases'                 => 'Fases',
        'dispositives'           => 'Dispositivos',
    ],
];
