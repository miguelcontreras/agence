@extends('mail.template')

@section('content')

	<h2>{!! $title !!}</h2>
	<p>{!! $content !!}</p>

@endsection
