<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>EMAIL DESDE {{ env('APP_NAME') }}</title>
    </head>
    <body>
        @yield('content')
    </body>
</html>