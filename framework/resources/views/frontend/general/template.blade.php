<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="es-ES">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js lt-ie9" lang="es-ES">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en-US">
<!--<![endif]-->
@include('frontend.general.html-header')
<body class="">
<div class="container-fluid">
    @include('frontend.general.header')

    <div class="row">
        <div class="col-md-12">
            <div id="container-box">
                @yield('content')
            </div>
        </div>
    </div>

    @include('frontend.general.footer')
</div>
@include('frontend.general.html-footer')
</body>
</html>
