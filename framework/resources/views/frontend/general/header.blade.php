<div class="row">
    <div class="col-md-12">
        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
            <TBODY>
            <TR>
                <TD width="100%">
                    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                        <TBODY>
                        <TR>
                            <TD style="BORDER-BOTTOM: #ccc 1px solid">&nbsp;</TD>
                            <TD width=98 background="" height=40 rowSpan=2>
                                <A href="http://www.agence.com.br/" target=_blank>
                                    <IMG alt="" src="{!! image_url('inc/logo.gif') !!}" border=0>
                                </A>
                            </TD>
                        </TR>
                        <TR>
                            <TD style="PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; BORDER-LEFT: #ccc 1px dotted; PADDING-TOP: 3px">
                                <IMG height=15 alt="" src="{!! image_url('inc/fig.gif') !!}" width=51 border=0>
                            </TD>
                        </TR>
                        </TBODY>
                    </TABLE>
                </TD>
            </TR>
            </TBODY>
        </TABLE>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="nav-menu">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#"><img src="{!! image_url('menu/agence.gif') !!}"> Agence</a></li>
            <li role="presentation"><a href="#"><img src="{!! image_url('menu/task_icon.gif') !!}"> Projetos</a></li>
            <li role="presentation"><a href="#"><img src="{!! image_url('menu/administrativo.gif') !!}"> Administrativo</a></li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="{!! image_url('menu/comercial.gif') !!}"> Comercial <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#"> Performance Comercial</a></li>
                </ul>
            </li>
            <li role="presentation"><a href="#"><img src="{!! image_url('menu/fichario.gif') !!}"> Financeiro</a></li>
            <li role="presentation"><a href="#"><img src="{!! image_url('menu/info_pessoa.gif') !!}"> Usuário</a></li>
            <li role="presentation"><a href="#" id="sign-out"><i class="fa fa-sign-out"></i> Sair</a></li>
        </ul>
    </div>
    <div class="col-md-1">
        <span id="btn-sign-out"></span>
    </div>
</div>
