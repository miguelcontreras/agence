<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="{{ $maker }}">
    <meta name="keywords" content="{{ isset($page_keywords) ? $page_keywords : 'Sin Palabras clave' }}"/>
    <meta name="description" content="{{ isset($page_description) ? $page_description : 'Sin Descripción' }}">
    @yield('metas')
    <title>{{ isset($page_title) ? $page_title : 'Sin Nombre' }} | {{ $site_name }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{!! image_url('favicon.ico') !!}"/>

    {{--FONTS--}}
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C600%2C700%2C800%2C300&#038;subset=latin%2Clatin-ext" type="text/css" media="all"/>--}}
    <link rel="stylesheet" href="{{ plugins_css_url_file('font-awesome/font-awesome.min.css', false) }}" type="text/css" media="all"/>

    {{--CSS--}}
    <link rel="stylesheet" href="{{ plugins_css_url_file('bootstrap-3.3.7/css/bootstrap.min.css', false) }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ plugins_css_url_file('sweetalert-master/dist-2/sweetalert.css', false) }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ plugins_css_url_file('datepicker/css/datepicker.css', false) }}" type="text/css" media="all"/>

    @yield('css_template_header')
    {!! css_url_file('styles.css')."\n" !!}
    @yield('css_header')

    <!--  JS  -->
    @yield('js_template_header')
    @yield('js_header')

    <script>if(window.top !== window.self){window.top.location.replace(window.self.location.href);}</script>
</head>
