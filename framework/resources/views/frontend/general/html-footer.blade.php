<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ plugins_js_url_file('jquery/jquery-1.11.3.min.js', false) }}"></script>
<script type="text/javascript" src="{{ plugins_js_url_file('bootstrap-3.3.7/js/bootstrap.min.js', false) }}"></script>
<script type="text/javascript" src="{{ plugins_js_url_file('sweetalert-master/dist-2/sweetalert.min.js', false) }}"></script>
<script type="text/javascript" src="{{ plugins_js_url_file('datepicker/js/bootstrap-datepicker.js', false) }}"></script>
<script type="text/javascript" src="{{ plugins_js_url_file('datepicker/js/locales/bootstrap-datepicker.es.js', false) }}"></script>

<script type="text/javascript" src="{{ js_url_file('scripts.js', false) }}"></script>

@include('sweet::alert')

@yield('js_template_footer')
@yield('js_footer')
