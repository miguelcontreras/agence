<graph bgColor="F1f1f1" caption="Performance Comercial" subCaption="{{ $months[substr('00'.$month_from, -2, 2)] }} de {{ $year_from }} a {{ $months[substr('00'.$month_to, -2, 2)] }} de {{ $year_to }}" showValues="0" divLineDecimalPrecision="2" formatNumberScale="2" limitsDecimalPrecision="2" PYAxisName="" SYAxisName="" decimalSeparator="," thousandSeparator="." SYAxisMaxValue="32000" PYAxisMaxValue="32000">
    <categories>
        @for($i = $month_from,$j = $year_from; $i <= $month_to || $j < $year_to; $i++)
            <category name="{{ substr($months[substr('00'.$i, -2, 2)], 0, 3) }}" hoverText="{{ $months[substr('00'.$i, -2, 2)] }}"/>

            @if($i == 12 && $j < $year_to)
                @php
                    $j++;
                    $i = 0;
                @endphp
            @endif
        @endfor
    </categories>
    @foreach($consultants as $consultant)
        <dataset seriesName="{{ $consultant->no_usuario }}" color="#{{ random_color() }}" numberPrefix="R$ ">
            @php
                $data = $consultant->serviceOrders->groupBy('year')->map(function($year){
                    return $year->groupBy('month')->toArray();
                })->toArray();
            @endphp

            @for($i = $month_from,$j = $year_from; $i <= $month_to || $j < $year_to; $i++)
                <set value="{{ isset($data[$j][$i][0]['net_income']) ? $data[$j][$i][0]['net_income'] : 0 }}"/>
                @if($i == 12 && $j < $year_to)
                    @php
                        $j++;
                        $i = 0;
                    @endphp
                @endif
            @endfor
        </dataset>
    @endforeach

    <dataset lineThickness="3" seriesName="Custo Fixo M�dio" numberPrefix="R$ " parentYAxis="S" color="FF0000" anchorBorderColor="FF8000">
        @for($i = $month_from,$j = $year_from; $i <= $month_to || $j < $year_to; $i++)
            <set value="{{ $avg_fixed_cost }}"/>
            @if($i == 12 && $j < $year_to)
                @php
                    $j++;
                    $i = 0;
                @endphp
            @endif
        @endfor
    </dataset>
</graph>
