@extends('frontend.general.template')

@section('content')
    {{--

    No redefini mucho el HTML a nivel visual puesto que como comenté no suelo crear diseños desde 0 sin nada que guiarme,
    pero lo que si hice fué tomar el diseño casi original del mismo y pasarlo a un código MUCHO más limpio.
    no me gusta trabajar desordenadamente.

    --}}

    <div id="tabs">
        <ul class="nav nav-pills">
            <li role="presentation" class="active"><a href="#content-by-consultant">Por Consultor</a></li>
            <li role="presentation"><a href="#content-by-client">Por Cliente</a></li>
        </ul>
        <div id="content-by-consultant">
            <table class="table table-responsive" id="pesquisaAvancada">
                <tbody>
                <tr bgcolor="#fafafa">
                    <td width="10%" nowrap="nowrap" bgcolor="#efefef">
                        <div align="right"><b>Período</b></div>
                    </td>
                    <td>
                        <div class="input-daterange">
                            <select name="select5" class="form-control" id="month-from">
                                <option value="01">Jan</option>
                                <option value="02">Fev</option>
                                <option value="03">Mar</option>
                                <option value="04">Abr</option>
                                <option value="05">Mai</option>
                                <option value="06">Jun</option>
                                <option value="07">Jul</option>
                                <option value="08">Ago</option>
                                <option value="09">Set</option>
                                <option value="10">Out</option>
                                <option value="11">Nov</option>
                                <option value="12">Dez</option>
                            </select>
                            <select name="select" class="form-control" id="year-from">
                                <option>2003</option>
                                <option>2004</option>
                                <option>2005</option>
                                <option>2006</option>
                                <option selected>2007</option>
                            </select>
                            a
                            <select name="select3" class="form-control" id="month-to">
                                <option value="01">Jan</option>
                                <option value="02">Fev</option>
                                <option value="03">Mar</option>
                                <option value="04">Abr</option>
                                <option value="05">Mai</option>
                                <option value="06">Jun</option>
                                <option value="07">Jul</option>
                                <option value="08">Ago</option>
                                <option value="09">Set</option>
                                <option value="10">Out</option>
                                <option value="11">Nov</option>
                                <option value="12">Dez</option>
                            </select>
                            <select name="select4" class="form-control" id="year-to">
                                <option>2003</option>
                                <option>2004</option>
                                <option>2005</option>
                                <option>2006</option>
                                <option selected>2007</option>
                            </select>
                        </div>
                    </td>
                    <td width="20%" rowspan="2">
                        <div id="report-types">
                            <button type="button" class="btn btn-default btn-block" onclick="executeAction('report');">
                                <img src="{{ image_url('icone_relatorio.png') }}"> Relat&oacute;rio
                            </button>
                            <button type="button" class="btn btn-default btn-block" onclick="executeAction('graph');">
                                <img src="{{ image_url('icone_grafico.png') }}"> Gr&aacute;fico
                            </button>
                            <button type="button" class="btn btn-default btn-block" onclick="executeAction('pizza');">
                                <img src="{{ image_url('icone_pizza.png') }}"> Pizza
                            </button>
                        </div>
                    </td>
                </tr>
                <tr bgcolor="#fafafa">
                    <td nowrap="nowrap" bgcolor="#efefef">
                        <div align="right"><b>Consultores</b></div>
                    </td>
                    <td>
                        <table align="center">
                            <tr>
                                <td>
                                    <select class="form-control" multiple size="8" name="list1" id="list1" style="width:280px">
                                        @foreach($consultants as $consultant)
                                            <option value="{{ $consultant->co_usuario }}">{{ $consultant->no_usuario }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-default btn-block" onclick="move(list1,list2);">
                                        <i class="fa fa-angle-double-right"></i></button>
                                    <button type="button" class="btn btn-default btn-block" onclick="move(list2,list1);"><i class="fa fa-angle-double-left"></i>
                                    </button>
                                </td>
                                <td>
                                    <select class="form-control" multiple size="8" name="list2" id="list2" style="width:280px">
                                    </select>
                                    <input type="hidden" name="lista_analista" value="">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <div id="response-by-consultant"></div>
        </div>
        <div id="content-by-client">En Construcción</div>
    </div>
@endsection

@section('js_footer')
    <script>
        function executeAction(action) {
            var data = {
                range: getRangeDate(),
                consultants: getSelected()
            };

            if(data.consultants.length > 0){
                switch(action){
                    case 'report':
                        $.post('<?=route('consultant.report');?>', data, function(data) {
                            if(data.success){
                                $('#response-by-consultant').html(data.html).show();
                            }
                        })
                            .fail(ajaxError);
                        break;
                    case 'graph':
                        $('#response-by-consultant').html('' +
                            '<table width="600" border="0" cellspacing="0" cellpadding="2" align="center">' +
                            '    <tr>' +
                            '        <td colspan="3" width="100%" style="BORDER:1px #D5DFE0 solid;">' +
                            '            <center>' +
                            '                <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="600" HEIGHT="350" id="FusionCharts">' +
                            '                    <PARAM NAME=movie VALUE="{{ url('assets/charts/FC_2_3_MSColumnLine_DY_2D.swf') }}">' +
                            '                    <PARAM NAME="FlashVars" VALUE="&dataURL={{ url('consultant/data_line_bar.xml') }}?consultants=' + data.consultants.join(',') + ',' + data.range.from + ',' + data.range.to + '&amp;chartWidth=600&amp;chartHeight=350">' +
                            '                    <PARAM NAME=quality VALUE=high>' +
                            '                    <PARAM NAME=bgcolor VALUE=#FFFFFF>' +
                            '                    <EMBED src="{{ url('assets/charts/FC_2_3_MSColumnLine_DY_2D.swf') }}" FlashVars="&dataURL={{ url('consultant/data_line_bar.xml') }}?consultants=' + data.consultants.join(',') + ',' + data.range.from + ',' + data.range.to + '&amp;chartWidth=600&amp;chartHeight=350" quality=high bgcolor=#FFFFFF WIDTH="600" HEIGHT="350" NAME="FusionCharts" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>' +
                            '                </OBJECT>' +
                            '            </center>' +
                            '        </td>' +
                            '    </tr>' +
                            '</table>' +
                            '').show();
                        break;
                    case 'pizza':
                        $('#response-by-consultant').html('' +
                            '<table width="600" border="0" cellspacing="0" cellpadding="2" align="center">' +
                            '    <tr>' +
                            '        <td colspan="3" width="100%" style="BORDER:1px #d5dfe0 solid;">' +
                            '            <center>' +
                            '                <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="600" HEIGHT="350" id="FusionCharts">' +
                            '                    <PARAM NAME=movie VALUE="{{ url('assets/charts/FC_2_3_Pie3D.swf') }}">' +
                            '                    <PARAM NAME="FlashVars" VALUE="&dataURL={{ url('consultant/data_pizza.xml') }}?consultants=' + data.consultants.join(',') + ',' + data.range.from + ',' + data.range.to + '&amp;chartWidth=600&amp;chartHeight=350">' +
                            '                    <PARAM NAME=quality VALUE=high>' +
                            '                    <PARAM NAME=bgcolor VALUE=#FFFFFF>' +
                            '                    <EMBED src="{{ url('assets/charts/FC_2_3_Pie3D.swf') }}" FlashVars="&dataURL={{ url('consultant/data_pizza.xml') }}?consultants=' + data.consultants.join(',') + ',' + data.range.from + ',' + data.range.to + '&amp;chartWidth=600&amp;chartHeight=350" quality=high bgcolor=#FFFFFF WIDTH="600" HEIGHT="350" NAME="FusionCharts" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>' +
                            '                </OBJECT>' +
                            '            </center>' +
                            '        </td>' +
                            '    </tr>' +
                            '</table>' +
                            '').show();
                        break;
                }
            }
            else{
                sweetAlert({
                    type: 'error',
                    title: '¡Deben haber items seleccionados!'
                });
            }
        }

        function getSelected() {
            return $('#list2 > option').map(function() {
                return $(this).val();
            }).get();
        }

        function getRangeDate() {
            /* var from = $('#datepicker-from').val().split('/');
             var to = $('#datepicker-to').val().split('/');

             return {
                 from: from[2] + '-' + from[1] + '-' + from[0] + ' 00:00:00',
                 to: to[2] + '-' + to[1] + '-' + to[0] + ' 23:59:59'
             };*/

            var to = new Date($('#year-to').val() + '-' + $('#month-to').val() + '-01 00:00:00');
            to.setMonth(to.getMonth() + 1);
            to.setDate(1);
            to.setDate(to.getDate() - 1);

            return {
                from: $('#year-from').val() + '-' + $('#month-from').val() + '-01 00:00:00',
                to: to.getFullYear() + '-' + getStringNumber(to.getMonth() + 1) + '-' + to.getDate() + ' 23:59:59',
            };
        }

        function ajaxError(xhr, status, error) {
            sweetAlert({
                type: 'error',
                title: '¡Ha ocurrido un error!'
            });

            $('#response-by-consultant').hide();
        }
    </script>
@endsection
