@foreach($consultants as $consultant)
    <table id="report-table">
        <tbody>
        <tr bgcolor=#efefef>
            <td colspan=5><span class="style3">{{ $consultant->no_usuario }}</span></td>
        </tr>
        <tr bgcolor=#fafafa>
            <td nowrap>
                <div align="center"><strong>Per&iacute;odo</strong></div>
            </td>
            <td>
                <div align="center"><strong>Receita L&iacute;quida </strong></div>
            </td>
            <td>
                <div align="center"><strong>Custo Fixo </strong></div>
            </td>
            <td>
                <div align="center"><strong>Comiss&atilde;o</strong></div>
            </td>
            <td>
                <div align="center"><strong>Lucro</strong></div>
            </td>
        </tr>
        @foreach($consultant->serviceOrders as $service_order)
            <tr bgcolor=#fafafa>
                <td nowrap>{{ $months[substr('00'.$service_order->month, -2, 2)] }} de {{ $service_order->year }}</td>
                <td>
                    <div align="right">R$ {{ number_format($service_order->net_income, 2, ',', '.') }}</div>
                </td>
                <td>
                    <div align="right">- R$ {{ number_format($service_order->fixed_cost, 2, ',', '.') }}</div>
                </td>
                <td>
                    <div align="right">- R$ {{ number_format($service_order->commission, 2, ',', '.') }}</div>
                </td>
                <td>
                    <div align="right">
                        @if($service_order->net_income - ($service_order->fixed_cost + $service_order->commission) < 0)
                            <font color="#FF0000">- R$ {{ number_format(abs($service_order->net_income - ($service_order->fixed_cost + $service_order->commission)), 2, ',', '.') }}</font>
                        @else
                            R$ {{ number_format(abs($service_order->net_income - ($service_order->fixed_cost + $service_order->commission)), 2, ',', '.') }}
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach

        <tr bgcolor=#efefef>
            <td nowrap bgcolor="#efefef"><strong>SALDO</strong></td>
            <td>
                <div align="right"><font color="#000000">R$ {{ number_format($consultant->serviceOrders->sum('net_income'), 2, ',', '.') }}</font></div>
            </td>
            <td>
                <div align="right"><font color="#000000">- R$ {{ number_format($consultant->serviceOrders->sum('fixed_cost'), 2, ',', '.') }}</font></div>
            </td>
            <td>
                <div align="right"><font color="#000000">- R$ {{ number_format($consultant->serviceOrders->sum('commission'), 2, ',', '.') }}</font></div>
            </td>
            <td>
                <div align="right">
                    @if($consultant->serviceOrders->sum('net_income') - ($consultant->serviceOrders->sum('fixed_cost') + $consultant->serviceOrders->sum('commission')) >= 0)
                        <font color="#0000FF">R$ {{ number_format(abs($consultant->serviceOrders->sum('net_income') - ($consultant->serviceOrders->sum('fixed_cost') + $consultant->serviceOrders->sum('commission'))), 2, ',', '.') }}</font>
                    @else
                        <font color="#FF0000">- R$ {{ number_format(abs($consultant->serviceOrders->sum('net_income') - ($consultant->serviceOrders->sum('fixed_cost') + $consultant->serviceOrders->sum('commission'))), 2, ',', '.') }}</font>
                    @endif
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <br>
@endforeach
