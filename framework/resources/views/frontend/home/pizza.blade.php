<graph caption="Participa��o na Receita" bgColor="F1f1f1" decimalPrecision="1" showPercentageValues="1" showNames="1" numberPrefix="" showValues="1" showPercentageInLabel="1" pieYScale="45" pieBorderAlpha="40" pieFillAlpha="70" pieSliceDepth="15" pieRadius="100">
    @foreach($consultants as $consultant)
        <set value="{{ $total != 0 ? $consultant->serviceOrders->sum('net_income')*100/$total : 0 }}" name="{{ $consultant->no_usuario }}" color="{{ random_color() }}" />
    @endforeach
</graph>
